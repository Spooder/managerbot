const Discord = require("discord.js");

module.exports.run = async (client,message,args) => {
  let role = message.guild.roles.find(r => r.name === "BanBot");
  if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");

    let rUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!rUser) return message.channel.send("Couldn't find user.");
    let rreason = args.join(" ").slice(22);

    let reportEmbed = new Discord.RichEmbed()
    .setAuthor("REPORT")
    .setColor("#d81d04")
    .addField("Reported User", `${rUser} with ID: ${rUser.id}`)
    .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
    .addField("Channel", message.channel)
    .addField("Time", message.createdAt)
    .setTimestamp()
    .addField("Reason", rreason);

    let reportschannel = message.guild.channels.find(`name`, "logs");
    let reporteduser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!reportschannel) return message.channel.send("Couldn't find reports channel, make a `logs` channel");
    let reportsstaff = message.guild.channels.find(`name`, "staff-chat");
    if(!reportsstaff) return message.channel.send("Couldn't find reports channel, make a `staff-chat` channel");
    
    let reportbackEmbed = new Discord.RichEmbed()
    .setAuthor("REPORT")
    .setDescription(`Thank you ${message.author} your report has been sent here are some of the details..`)
    .setColor("#ce790a")
    .addField("Reported User", `${rUser} with ID: ${rUser.id}`)
    .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
    .addField("Channel", message.channel)
    .addField("Time", message.createdAt)
    .setTimestamp()
    .setFooter("Forked Thanks To OfficialGaming ")
    .addField("Reason", rreason);

    let reporteduserEmbed = new Discord.RichEmbed()
    .setAuthor("REPORT")
    .setDescription(`Your have been reported!`)
    .setColor("#ce790a")
    .addField("Channel", message.channel)
    .addField("Time", message.createdAt)
    .addField("How to get this report remove?","If you want this report gone ask a staff member.")
    .setFooter("This message is an official message from Dream-MC")
    .addField("Reason", rreason);

    let reportstaffEmbed = new Discord.RichEmbed()
    .setAuthor("REPORT")
    .setColor("#d81d04")
    .addField("Reported User", `${rUser}`)
    .addField("Reported By", `${message.author}`)
    .addField("Channel", message.channel)
    .addField("Time", message.createdAt)
    .addField("Reason", rreason);

    message.delete().catch(O_o=>{});
    reportschannel.send(reportEmbed);
    reportsstaff.send(reportstaffEmbed);
    reporteduser.send(reporteduserEmbed); 
    message.channel.send(reportbackEmbed).then(msg => msg.delete(20000));

    return;
}
module.exports.help = {
  name: "report"
}
//author.send
//    let reporteduser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
//reporteduser.send(reporteduserEmbed); 