const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let role = message.guild.roles.find(r => r.name === "BanBot");
    if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");
    let icon = message.author.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor(message.author.username)
        .setDescription("Your Voice Channel Settings")
        .setColor("#5499C7")
        .setThumbnail(icon)
        .addField("Server Mute", message.member.serverMute)
        .addField("Server Deafen", message.member.serverDeaf)
        .addField("Self Mute", message.member.selfMute)
        .addField("Self Deafen", message.member.selfDeaf)
        .addField("Speaking", message.member.speaking)
      
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(10000)});
}

module.exports.help = {
  name:"voice"
}
