const Discord = require("discord.js");
const errors = require("../utils/errors.js");

module.exports.run = async (bot, message, args) => {
  let role = message.guild.roles.find(r => r.name === "BanBot");
  if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");

  message.delete();
  if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.reply("You Need perms: MANAGE_MESSAGES");
  let botmessage = args.join(" ");
    message.channel.send(botmessage);
}

module.exports.help = {
  name: "say"
}
