const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let role = message.guild.roles.find(r => r.name === "BanBot");
    if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");
    let bicon = bot.user.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor("  ")
        .setDescription(":mega: Dream-MC Rules")
        .setColor("#ed1a1a")
        .setThumbnail(bicon)
        .setFooter(`www.Dream-MC.net || !Help - For Help Commands!`, `${message.guild.iconURL}`)
        .addField("Discord Rules:","Hello! Please [**Click Here**](https://www.dream-mc.net/index.php?route=/forum/topic/2-discord-rules) for your discord rules. \n\ ** **")
        .addField(":mega: Extra info!","Want to get cool items and take part in giveaways and get updates? \n\Join are [**Dream-MC**](https://www.dream-mc.net/register) site or [**Click Here**](https://www.dream-mc.net/register) to register on are site and take part in a **giveaway** for a **FREE** rank!")
        
    message.channel.send({embed: embed})
    .then(msg => {msg.delete(5000)});

}

module.exports.help = {
  name:"rules"
}
