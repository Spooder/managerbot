/*const Discord = require("discord.js");

exports.run = (client, message, args, level) => {

  let question = args.slice(0).join(" ");
  let pollchan = message.guild.channels.find(`name`, "polls");

  if (args.length === 0)
  return message.reply('**Invalid Format:** `!Poll <Question>`')

  const embed = new Discord.RichEmbed()
  .setTitle("A Poll Has Been Started!")
  .setColor("#0916c4")
  .setDescription(`${question} \n\n\ Yes: 👍 \n No: 👎 \n Maybe: 🤷`)
  .setTimestamp()
  .setFooter(`Poll Started By: ${message.author.username} v1.0 `, `${message.author.avatarURL}`)

  pollchan.send({embed})
  .then(() => pollchan.message.react('👍'))
  .then(() => pollchan.message.react('👎'))
  .then(() => pollchan.message.react('🤷'))
  .catch(() => console.error('Emoji failed to react.'));

}

module.exports.help = {
    name:"poll"
  }
  */
 const Discord = require('discord.js');
exports.run = async (client, message, args, tools) => {
  let role = message.guild.roles.find(r => r.name === "BanBot");
  if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");
  
  const Discord = require("discord.js");
  const kitty = message.guild.members.find("id", "131417543888863232");
  if (message.member !== kitty)return message.channel.send("**Hey, `" + message.author.username + "` You Can't use polls. Only spoodercraft can!**").then(msg => msg.delete(7000));


  
    let question = args.slice(0).join(" ");
    let pollchan = message.guild.channels.find(`name`, "polls");

    if (args.length === 0)
    return message.reply('**Invalid Format:** `!poll <Question>`')
  
    const embed = new Discord.RichEmbed()
    .setTitle("@everyone A Poll Has Been Started!")
    .setColor("#ce790a")
    .setDescription(`**${question}** \n **YES**: 👍 -=- **NO**: 👎 -=- **MAYBE**: 🤷`)
    .setFooter(`Poll Started By: ${message.author.username}`, `${message.author.avatarURL}`)
    const pollTitle = await pollchan.send({ embed });
    pollchan.send({pollTitle})
    pollTitle.react('👍')
    .then(() => pollTitle.react('👎'))
    .then(() => pollTitle.react('🤷'))
    .catch(() => console.error('Emoji failed to react.'));
  
  

}

module.exports.help = {
  name:"poll"
}