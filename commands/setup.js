const Discord = require("discord.js");
const errors = require("../utils/errors.js");

module.exports.run = async (bot, message, args) => {
    if(!message.member.hasPermission("ADMINISTRATOR")) return message.channel.send("Only a user with the Perm ADMINISTRATOR can do this command!");
    let starttext = new Discord.RichEmbed()
    .setDescription("HELLO! Thank you for using me! I will be making some roles to make me work! \nThe ADMINISTRATOR must add the roles to the users that have it. \nStaff - Has permissions to ban, mute, and other things. \nMute - Can not talk. \nBanBot - Makes the user not allowed to use me! \nPlease wait 1-2 seconds before doing an command! Thank you!")
    .setColor("#05bf12");

    let botbanrole = message.guild.roles.find(r => r.name === "BanBot");
    if(!botbanrole) {
        try{
           role = await message.guild.createRole({
             name: "BanBot",
             color: "#000000",
            preissions: [
            "USE_EXTERNAL_EMOJIS"
        ]});
      } catch(e) {
          console.log(e.stack);
      }

      //start new
      let muterole = message.guild.roles.find(r => r.name === "Muted");
      if(!muterole) {
          try{
             role = await message.guild.createRole({
               name: "Muted",
               color: "#000000",
              preissions: [
                "SEND_MESSAGES",
                "ADD_REACTIONS"
              ]});
        } catch(e) {
            console.log(e.stack);
        }
    }

    let staffrole = message.guild.roles.find("name", "Staff");
    if(!staffrole) {
        try{
           role = await message.guild.createRole({
             name: "Staff",
             color: "#000000",
            permissions: [
            "KICK_MEMBERS",
            "BAN_MEMBERS",
            "MANAGE_MESSAGES",
            "MUTE_MEMBERS"//,
            //"MANAGE_MEMBERS"
            ]});
      } catch(e) {
          console.log(e.stack);
      }
  }

  }

  let welcomemsgEmbed = new Discord.RichEmbed()
  .addField("HELLO! EVERYONE!", `Thank you for inviting me! The ADMINISTRATOR must do some things before i start working!`)
  .addField("1",`Create a channel called **logs** that I can access! **AND FOR SPOODERCRAFT SINCE I AM IN BETA!**`)
  .addField("2", `Create channel called **staff-chat** that your staff can access and that i can access!`)
  .addField("3", `Then once that is all done please do **MB.newsetup** so i can make the roles!`)
  .addField("4", `Add a role called Staff that I made to all the Staff Members so they can do my commands!`)
  .addFiled("5", `Make sure to remember im in beta so i update everyday do **MB.update** at sometime to see what is updated! Or watch my status to see if i show **NEW UPDATE**!`)
  .addField("6", `Thats it im all done!`)
  .addField("PLEASE NOTE" , `I do not have an help command since the help command is taking some time to work. SpooderCraft is in this discord please tag him or pm him if you want to know some of my commands`)
  .addField("BUG REPORT SYSTEM", `I have an bugreport command (command: MB.bugreport (type of bug) (msg)) This command sends SpooderCraft the details of your msg so he can fix the bug! Please do not spam this command!`)
  .addField("Command Cooldown System", `I have a command cooldown of 5 seconds so people can not spam my command!`)
  .addField("Bot Version", `**${version}**`)
  .setColor("#ce790a")
  .setTimestamp()
  .setFooter("Bot Made By SpooderCraft | Dream-MC#3911");

  message.channel.send({embed: welcomemsgEmbed});
}

module.exports.help = {
    name:"newsetup"
  }
