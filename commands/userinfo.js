const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let role = message.guild.roles.find(r => r.name === "BanBot");
    if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");
    let icon = message.author.displayAvatarURL;
    let embed = new Discord.RichEmbed()
        .setAuthor(message.author.username)
        .setDescription("This Is " + message.author.username + " User Infomation")
        .setColor("#ce790a")
        .setThumbnail(icon)
        .addField("Full Username", message.author.tag)
        .addField("ID", message.author.id)
        .addField("Server Mute", message.member.serverMute)
        .addField("Server Deafen", message.member.serverDeaf)
        .addField("Self Mute", message.member.selfMute)
        .addField("Self Deafen", message.member.selfDeaf)
        .addField("Speaking", message.member.speaking)
      
    message.channel.send({embed: embed});
}

module.exports.help = {
  name:"userinfo"
}
