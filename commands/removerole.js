const Discord = require("discord.js");

module.exports.run = async (client,message,args) => {
  let crole = message.guild.roles.find(r => r.name === "BanBot");
  if(message.member.roles.has(crole.id)) return message.channel.send("Can not use this commnad!");

    if(!message.member.hasPermission("MANAGE_ROLES")) return message.reply("Sorry, you do not have permission to change ranks!");
    let rMember = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);
    if(!rMember) return message.reply("Couldn't find that user");
    let role = args.join(" ").slice(22);
    if(!role) return message.reply("Specify a role!");
    let gRole = message.guild.roles.find(`name`, role);
    if(!gRole) return message.reply("Couldn't find that role.");
    let logchannel = message.guild.channels.find(`name`, "logs");
    if(!logchannel) return message.channel.send("Couldn't find reports channel, make a `logs` channel");
  
    const remEmbed = new Discord.RichEmbed()
    .setTitle("RANK REMOVED")
    .setColor("#d81d04")
    .addField("Role Added", `${gRole.name}`)
    .addField("Role Added From",`${message.author.username}`)
    .setFooter(`Message Sent From Dream-MC Discord`);
    if(!rMember.roles.has(gRole.id)) return message.reply("Done.");
    await(rMember.removeRole(gRole.id));
  
    try{
      await rMember.send(remEmbed)
      console.log(`${message.author.username} removed role ${gRole.name} to ${rMember}`)
      logchannel.send(remEmbed)
    }catch(e){
      console.log(e.stack);
      message.channel.send(` ${gRole.name} was removed from ${rMember}`)
    }
    message.channel.send(`${gRole.name} was removed from ${rMember}`)
}

module.exports.help = {
  name: "delrole"
}