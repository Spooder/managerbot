const Discord = require("discord.js");
const moment = require("moment")
require("moment-duration-format");

module.exports.run = async (client,message,args) => {
    let role = message.guild.roles.find(r => r.name === "BanBot");
    if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");

    const duration = moment.duration(client.uptime).format(" D [Days], H [Hours], m [minutes], s [Seconds]");
    let bicon = client.user.displayAvatarURL;
        let uptimeembed = new Discord.RichEmbed()


        .setThumbnail(bicon)
        .setTitle(`ManagerBot\`s Uptime :eyes:`)
        .setColor("#a7f442")
        .setTimestamp()
        .setDescription(duration);
    
    message.channel.send(uptimeembed).then(msg => msg.delete(11000));


}
module.exports.help = {
    name: "uptime"
}