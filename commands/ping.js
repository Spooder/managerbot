const Discord = require("discord.js");

exports.run = async (client, message) => {
  let role = message.guild.roles.find(r => r.name === "BanBot");
  if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");
    const msg = await message.channel.send("Pinging...");
    await msg.edit(`Your Ping\n\ ${msg.createdTimestamp - message.createdTimestamp}ms.)`).then(msg => {msg.delete(10000)});
};
  
exports.conf = {
  enabled: true,
  runIn: ["text"],
  aliases: [],
  permLevel: 0,
  botPerms: [],
  requiredFuncs: [],
};
  
exports.help = {
  name: "ping",
  description: "Ping/Pong command.",
  usage: "",
  usageDelim: "",
};

// const Discord = require("discord.js");

// module.exports.run = async (bot, message, args) => {
//         message.channel.send({embed:{
//                 color: 0x00ff15,
//                 description: "" + message.author + " :ping_pong: Pong"

//             }
//         });
// }

// module.exports.help = {
//   name:"ping"
// }  
