const Discord = require("discord.js");

module.exports.run = async (client,message,args) => {
  let crole = message.guild.roles.find(r => r.name === "BanBot");
  if(message.member.roles.has(crole.id)) return message.channel.send("Can not use this commnad!");


    if(!message.member.hasPermission("MANAGE_ROLES")) return message.reply("Sorry, you do not have permission to change ranks!");
    let rMember = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);
    if(!rMember) return message.reply("Couldn't find that user");
    let role = args.join(" ").slice(22);
    if(!role) return message.reply("Specify a role!");
    let gRole = message.guild.roles.find(`name`, role);
    if(!gRole) return message.reply("Couldn't find that role.");
    let logchannel = message.guild.channels.find(`name`, "logs");
    if(!logchannel) return message.channel.send("Couldn't find reports channel, make a `logs` channel");

    const addedEmbed = new Discord.RichEmbed()
    .setTitle("RANK ADDED")
    .setColor("#05bf12")
    .addField("Role Removed", `${gRole.name}`)
    .addField("Role Removed From",`${message.author.username}`)
    .setFooter(`Message Sent From Dream-MC Discord`);
    if(rMember.roles.has(gRole.id)) return message.reply("They already have that role.");
    await(rMember.addRole(gRole.id));
  
    try{
      await rMember.send(addedEmbed)
      console.log(`${message.author.username} added role ${gRole.name} to ${rMember}`)
      logchannel.send(addedEmbed)
    }catch(e){
      console.log(e.stack);
      message.channel.send(`${gRole.name} was given to ${rMember}`)
    }
    message.channel.send(`${gRole.name} was given to ${rMember}`)
}
    
module.exports.help = {
  name: "addrole"
}
//`${message.author.username} added ${gRole.name} to your user!`