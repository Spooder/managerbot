const Discord = require("discord.js");
const client = new Discord.Client();

module.exports.run = async (client, message, args, bot) => {

    let bugrreasontype = message.content.split(" ").slice(1, 2).join(" ");
    let bugrreason = message.content.split(" ").slice(2).join(" ");
    let botowner = client.users.find("id", "131417543888863232");
	
message.guild.fetchInvites()
      .then(invites => {
      let onrinv = invites.find(invite => invite.inviter.id === message.guild.owner.id === true)
      let chan = message.guild.channels.find("name","general");
	  if (onrinv) {
	  let mbed = new Discord.RichEmbed()
	  .setAuthor("Bug Report")
      .setColor("#ce790a")
      .addField("Submmited User", `${message.author}`)
      .addField("Submmited User ID", `${message.author.id}`)
      .addField("Guild", `${message.guild.name}`)
	  .addField('Invite link:', `${onrinv}`)
      .addField("Time", message.createdAt)
      .addField("Bug Type", bugrreasontype)
      .addField("Reason", bugrreason);
      botowner.send({embed: mbed})
	  }
	  if (!onrinv){
		chan.createInvite({ maxAge: 0 })
	  .then(invite => {
	  let mbed = new Discord.RichEmbed()
	  .setAuthor("Bug Report")
      .setColor("#ce790a")
      .addField("Submmited User", `${message.author}`)
      .addField("Submmited User ID", `${message.author.id}`)
      .addField("Guild", `${message.guild.name}`)
	  .addField('Invite link:', `${invite}`)
      .addField("Time", message.createdAt)
      .addField("Bug Type", bugrreasontype)
      .addField("Reason", bugrreason);
      botowner.send({embed: mbed})
	  })
	  }
	})
}
module.exports.help = {
    name: "bug"
  }