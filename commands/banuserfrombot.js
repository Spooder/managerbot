const Discord = require("discord.js");
const errors = require("../utils/errors.js");
/*this command is try to give the user a role witch the other commands would have something like "if(message.member.role("BanBot")) return message.reply("Your Banned From Using Me!");" try to make it stop the user if the user has the role from doing that command */ 

module.exports.run = async (bot, message, args) => {
        if(message.author.id !== "131417543888863232") return message.channel.send("**Hey, `" + message.author.username + "` That Command Only Works For The BotOwner**")
//        if (!message.member.permissions.has("MANAGE_MESSAGES")) return message.channel.send("**Hey, `" + message.member.displayName + "` You Do Not Have Enough Perms To Mute or You Need The Perm `MANAGE_MESSAGE`!**");


        let toMute = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);
        if(!toMute) return message.channel.send("**Hey, `" + message.member.displayName + "` You Need To `MENTION USER` Please Try Again**");

        if(toMute.id === message.author.id) return message.channel.send("**Hey, `" + message.member.displayName + "` You Can't Ban Yourself From Using Me.**");
//        if(toMute.highestRole.position >= message.member.highestRole.position) return message.channel.send("**Hey, `" + message.member.displayName + "` You Can't Mute A Member That Has A `Better/Higher` Role As You**");

        let role = message.guild.roles.find(r => r.name === "BanBot");
        if(!role) {
            try{
               role = await message.guild.createRole({
                 name: "BotBan",
                 color: "#000000",
                preissions: []
              });

              message.guild.channels.forEach(async (channel, id) => {
                  await channel.overwritePermissions(role, {
                    USE_EXTERNAL_EMOJIS: false
                  });
              });
          } catch(e) {
              console.log(e.stack);
          }
      }
       
          if(toMute.roles.has(role.id)) return message.channel.send("**Hey, `" + message.member.displayName + "` This User Is `ALREADY` Banned from using me.**");

          await toMute.addRole(role);
          message.channel.send("**`" + message.member.displayName + "` The Deed is done! No More Of Using Me**");
          
    let banEmbed = new Discord.RichEmbed()
    .setDescription("~BOT BAN~")
    .setColor("#bc0000")
    .addField("Bot Banned User", `${toMute} with ID ${toMute.id}`)
    .addField("Bot Banned By", `<@${message.author.id}> with ID ${message.author.id}`)
    .addField("Bot Banned In", message.channel)
    .addField("Time", message.createdAt)

    let incidentchannel = message.guild.channels.find(`name`, "logs");
    if(!incidentchannel) return message.channel.send("Can't find incidents channel.");

    toMute.send(banEmbed);
    incidentchannel.send(banEmbed);
}

module.exports.help = {
  name:"botban"
}
