exports.run = async (client, message) => {
    let role = message.guild.roles.find(r => r.name === "BanBot");
    if(message.member.roles.has(role.id)) return message.channel.send("Can not use this commnad!");
    let y = Math.random();
  
    if (y > 0.5) { return message.channel.send("Heads!"); }
    else { return message.channel.send("Tails!"); }
};

exports.conf = {
    enabled: true,
    runIn: ["text"],
    aliases: [],
    permLevel: 0,
    botPerms: [],
};
      
exports.help = {
    name: "coin",
    description: "Flips a coin!",
    usage: "",
    usageDelim: "",
};