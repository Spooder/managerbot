const Discord = require("discord.js");
const botjs = require(`../bot.js`);
const client = new Discord.Client();
module.exports.run = async (client, message, args) => {
    let icon = message.author.displayAvatarURL
    let botico = client.user.displayAvatarURL
    let embed = new Discord.RichEmbed()
    .setTitle(`Stats for ${client.user.username}!`)
    .addField(" Mem Usage ✬",`**__${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB__**`)
    .addField(" Servers ✬", `**__${client.guilds.size}__**`)
    .addField(" Users ✬", `**__${client.users.size}__**`)
    .addField(" Channels ✬", `**__${client.channels.size}__**`)
    .addField(" Discord.js ✬", `**__v${Discord.version}__**`)
    .addField(" Node.js ✬" , `**__${process.version}__**`)
    .addField(" Bot Name ✬", client.user.username)
    .addField(" Created On ✬", client.user.createdAt)
    .setColor("#212121")
    .setTimestamp()
    .setFooter("Forked From OfficialGaming ")
    .setThumbnail(botico);

    message.channel.send({embed: embed});

}

module.exports.help = {
  name:"old-botstatus"
}
//Forked from OfficialGamingG

/*
        if(cmd === `${prefix}stats`){
            let bicon = client.user.displayAvatarURL;
            let statsEmbed = new Discord.RichEmbed()
            .setTitle(`Stats for ${client.user.username}`)
            .addField(`• Mem Usage •`,  `**__${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB__**`)
            .addField("• Servers •", `**__${client.guilds.size}__**`)
            .addField("• Users •", `**__${client.users.size}__**`)
            .addField("• Channels •", `**__${client.channels.size}__**`)
            .addField("• Discord.js •", `**__v${Discord.version}__**`)
            .addField("• Node.js •" , `**__${process.version}__**`)
            .setColor(embeds.defaultColor)
            .setTimestamp()
            .setThumbnail(bicon);
            
                message.channel.send(statsEmbed);

            
        }
*/
